from flask import Flask, render_template
import mysql.connector

app = Flask(__name__)

# Функція для підключення до бази даних та отримання даних з таблиці sys_config
def get_sys_config():
    # Параметри підключення до бази даних
    config = {
    'user': 'root',
    'password': '1',
    'host': '172.27.28.203',
    'database': 'sys',
    }

    # Підключення до бази даних
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()

    # Виконання запиту для отримання даних з таблиці sys_config
    query = 'SELECT * FROM sys_config'
    cursor.execute(query)

    # Отримання результатів запиту
    result = cursor.fetchall()

    # Закриття підключення до бази даних
    cursor.close()
    connection.close()

    return result

@app.route('/')
def index():
    # Отримання даних з таблиці sys_config
    sys_config_data = get_sys_config()

    # Передача даних на веб-сторінку
    return render_template('index.html', sys_config=sys_config_data)

if __name__ == '__main__':
    app.run('0.0.0.0',debug=True,port=8000)
